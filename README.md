### GitLab-kNN

A proof-of-concept classifier that can run in GitLab's pipelines. Uses a simple Ruby implementation of kNN to classify objects based on a training set, which grows with successful MRs to the project.

### Usage

Anything can be classified but currently the project uses the MNIST training set to classify images of **handwritten digits**.

To have an image classified:

1. Convert it to the [MNIST format in CSV];
1. Make an MR against this project, putting your file(s) in the `data/mnist/unclassified` directory;
1. The CI pipeline will run, attempting to apply a classification to the image (with about 98% accuracy);
1. A project maintainer reviews the pipeline, if the classification is correct the file is moved to the correct `data/mnist/train` subdirectory, and the classifier gets a little more clever; or
1. If the classification is incorrect, the maintainer still moves it to the correct `data/msint/train` subdirectory, and the classifier gets smarter yet...

### Usage outside of pipelines

The pipeline work isn't done yet. Until it is you can clone this project and run the classifier locally. If you don't have an image in the right format, you can simply take one at random from the `data/mnist/test` directory and place it in the `data/mnist/unclassified` directory. 

Then run:

```sh
bundle exec bin/classify
```

The program will take some time to run, then output something like:

```sh
Classified ~/gitlab-knn/data/mnist/unclassified/what-number-is-this.csv as 9
```


### Todo

- Automatically convert images to [MNIST format in CSV] in the pipeline.
- Apply the classification by automatically adding a commit to the MR moving the file to the correct training directory, so a maintainer only has to merge it if it's correct.

### Why kNN?

kNN has a lot of downsides when compared to support vector machines (SVN) or neural nets (NN), the major ones being that it's generally less accurate for this kind of task* and has a high upfront cost, as it's necessary to calculate the distance from every point in the training set.

The upside is that it's one of the easiest machine learning algorithms to build an intuition for, since it's based on Euclidian distance in _n_ dimensions and Pythagoras' Theorem is commonly taught in school mathematics courses.

The purpose of this project is just to show that it's possible to 'train' a machine learning algorithm using GitLab's existing CI pipelines and MR workflows. 



* Not necessarily though. Some kNN algorithims have beaten SVNs with the right preprocessing. See [the Wikipedia article](https://en.wikipedia.org/wiki/MNIST_database#classifiers).

### What is the MNIST format?

A grayscale, centered image of dimensions 28x28. In this project a single image is a CSV of one line containing 784, comma-separated numbers between 0 and 255. In other words, each pixel is represented by 1 byte.

[MNIST format in CSV]: #what-is-the-mnist-format
